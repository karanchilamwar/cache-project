import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { API, APIMetadataDTO } from '../dto/api-dto';
import { RemoteService } from '../service/remote.service';

@Component({
  selector: 'app-patient-summery',
  templateUrl: './patient-summery.component.html',
  styleUrls: ['./patient-summery.component.css']
})
export class PatientSummeryComponent {
   apiData:API[] = [];

   filterForm:FormGroup=new FormGroup({
    api:new FormControl(null),
    auth:new FormControl(null)
   })

   constructor(private remoteService:RemoteService){
        this.remoteService.getCatalog(324,"akdd").subscribe(data=>{
            this.apiData=data.entries;
        });
   }



}
