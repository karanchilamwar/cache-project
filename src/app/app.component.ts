import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NavigationStart, Router } from '@angular/router';
import { PostDTO } from './dto/post-dto';
import { KeyGenerator } from './service/key-generator.service';
import { RemoteService } from './service/remote.service';
import { StorageService } from './service/storage-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  posts:PostDTO[]=[];
  storageService:StorageService=new StorageService();
  keyGenerator:KeyGenerator=new KeyGenerator();
  form:FormGroup=new FormGroup({
    name:new FormControl(null),
    id:new FormControl(null)
  })

  constructor(private service:RemoteService,private router:Router){
    this.service.getPost(123,"abc").subscribe(data=>{
      this.posts=data;
    })

    this.router.events.subscribe(event=>{
      if (event instanceof NavigationStart) {
        if(!router.navigated){
          this.storageService.removeItem(this.keyGenerator.cacheKey.getValue());
        }
      }
    })
  }

}
