import { Component } from '@angular/core';
import { RemoteService } from '../service/remote.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent {

  cryptoPrices:any[]=[];

  constructor(private remoteService:RemoteService){
this.remoteService.getCrypto(3232,"434").subscribe(data=>{
     this.cryptoPrices=data;
})
  }
}
