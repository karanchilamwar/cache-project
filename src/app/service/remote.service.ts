import { HttpClient } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { APIMetadataDTO } from '../dto/api-dto';
import { PostDTO } from '../dto/post-dto';
import { Cacheable } from './decorator/cacheable';
import { Query } from './decorator/query-decorator';


@Injectable({
  providedIn: 'root'
})
@Cacheable("remoteService")
export class RemoteService {
  
  baseURL:string="https://reqres.in/api/";

  constructor(private httpClient:HttpClient) { }

@Query()
  getPost(id:number,name:string):Observable<PostDTO[]>{
      return this.httpClient.get<PostDTO[]>("https://jsonplaceholder.typicode.com/posts");
  }

  @Query()
  getCatalog(id:number,name:string):Observable<APIMetadataDTO>{
      return this.httpClient.get<APIMetadataDTO>("https://api.publicapis.org/entries");
  }

  @Query()
  getCrypto(id:number,name:string):Observable<any[]>{
      return this.httpClient.get<any[]>("https://archive.org/metadata/TheAdventuresOfTomSawyer_201303");
  }
}
