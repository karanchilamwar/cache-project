import { of } from "rxjs";
import { CacheDTO } from "src/app/dto/cache-dto";
import { KeyGenerator } from "../key-generator.service";
import { StorageService } from "../storage-service";


export function Query(){
    return function(target: Object, 
        propertyKey: string, 
        descriptor: TypedPropertyDescriptor<any>,...args:any):any{

           const originalMethod=descriptor.value;
           descriptor.value = function (...args: any) {
            const keyGenerator:KeyGenerator=new KeyGenerator();
            const storageService:StorageService=new StorageService();

            const generatedCacheKey=keyGenerator.createKey(target.constructor.name,propertyKey,args);
            let sessionServiceCache=storageService.getValue(keyGenerator.cacheKey.getValue());
         
            if(!sessionServiceCache){
               var cacheStore=new Map<string,any>();
            }else{
                var cacheStore=new Map<string,any>(JSON.parse(sessionServiceCache));
            }
            if(cacheStore.get(generatedCacheKey)){
                return of(JSON.parse(cacheStore.get(generatedCacheKey)));
            }else{
                const result = originalMethod.apply(this, args);
                result.subscribe((data: any)=>{
                     cacheStore.set(generatedCacheKey, JSON.stringify(data));
                     sessionStorage.setItem(keyGenerator.cacheKey.getValue(),JSON.stringify([...cacheStore]));
                })
               return result;
            }
         }
              return null;
        }
        
    }
