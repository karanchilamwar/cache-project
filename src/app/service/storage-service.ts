import { KeyGenerator } from "./key-generator.service";


export class StorageService{

keyGenerator:KeyGenerator=new KeyGenerator();

constructor(){

}

save(key:string,value:any):void{
    sessionStorage.setItem(key,value);
}

getValue(key:string):any{
  return sessionStorage.getItem(key);
}

removeItem(key:string):void{
  sessionStorage.removeItem(key);
}

}