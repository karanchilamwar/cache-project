import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";



export class KeyGenerator{
    
    cacheKey:BehaviorSubject<string>=new BehaviorSubject<string>("AAEJSSJJSNNCNDJJD");

    constructor(){

    }

    createKey( serviceName : string, methodName : string, ...args: any[]):string{
        return serviceName+""+methodName+""+args.map(args=>args+"").reduce((a:string,b:string)=>a+b);
    }

}