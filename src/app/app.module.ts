import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PatientJourneyComponent } from "./patient-journey/patient-journey.component";
import { PatientSummeryComponent } from "./patient-summery/patient-summery.component";
import { ReportsComponent } from "./reports/reports.component";

@NgModule({
  declarations: [
    AppComponent,
    PatientJourneyComponent,
    PatientSummeryComponent,
    ReportsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
