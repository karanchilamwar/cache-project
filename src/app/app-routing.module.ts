import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientJourneyComponent } from './patient-journey/patient-journey.component';
import { PatientSummeryComponent } from './patient-summery/patient-summery.component';
import { ReportsComponent } from './reports/reports.component';

const routes: Routes = [
  { path: '', redirectTo: 'pj', pathMatch: 'full' },
  { path: 'pj', component: PatientJourneyComponent },
  { path: 'ps', component: PatientSummeryComponent },
  { path: 're', component: ReportsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
