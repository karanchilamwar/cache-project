export interface APIMetadataDTO{
    count:number,
    entries:API[]
}

export interface API{
    API	:	string
Description	:	string
Auth	:	string
HTTPS	:	boolean
Cors	:	string
Link	:	string
Category	:	string

}