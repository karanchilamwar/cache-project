import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PostDTO } from '../dto/post-dto';
import { RemoteService } from '../service/remote.service';
import { StorageService } from '../service/storage-service';

@Component({
  selector: 'app-patient-journey',
  templateUrl: './patient-journey.component.html',
  styleUrls: ['./patient-journey.component.css']
})
export class PatientJourneyComponent {
  posts:PostDTO[]=[];

  form:FormGroup=new FormGroup({
    name:new FormControl(null),
    id:new FormControl(null)
  })

  constructor(private service:RemoteService){
    this.service.getPost(123,"abc").subscribe(data=>{
      this.posts=data;
    })
  }

  Filter(){
    debugger
    this.service.getPost(this.form.value.name,this.form.value.id).subscribe(data=>{
      this.posts=data.filter(data=>data.userId==this.form.value.id);
    })
  }

}
